---
layout: default
title: Volunteer!
permalink: /volunteer/
date: '2018-05-15 23:39:31 -0400'
---
# Volunteer!

Let's be real for a minute. Keeping 20+ people together on a bike ride can be difficult to do alone. Sometimes you can't get everyone through a red light fast enough without corking the intersection, sometimes a rider breaks down or falls behind and ends up dropped, sometimes riders are inexperienced and slow, sometimes no one knows who's leading and confusion causes a potentially dangerous situation. One of the reasons we've started Flower City Bike Party is due to years of experience helping to organize similar rides efficiently and safely which considers these issues and takes steps to avoid them.

To do that effectively, we need volunteers willing to cork intersections, assist in leading the ride, making sure we don't lose anyone, stopping the ride if someone breaks down, etc. We could also use a graphic designer and/or frontend developer. In the future, we'll have a form on this page you can fill out below. Until then, come out on a ride and chat with an organizer.
