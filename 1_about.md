---
layout: default
title: What is Flower City Bike Party?
permalink: /about/
date: '2018-05-15 21:17:07 -0400'
---
# What is Flower City Bike Party?
Flower City Bike Party is a monthly bicycle party ride in downtown Rochester, NY (inspired [by the wonderful people in Washington, DC](https://www.facebook.com/DCBikeParty/)).

The downtown Rochester cycling scene is growing and much like the city itself, it is growing rapidly. Flower City Bike Party's goal is to be a positive part of this growth. Once a month, we will explore the city via bicyle with a new route and new post-ride bar. Everyone with a bicycle (or tricycle, we love all human powered vehicles) is welcome and the ride will always be free. All post-ride outings are 21+ (please don't make us enforce this). Hop on a bike, make some new friends, and enjoy the ride!
