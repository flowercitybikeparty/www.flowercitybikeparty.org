---
layout: default
title: What is a Bike Party?
permalink: /whatisabikeparty/
date: '2018-07-19 08:43:23 -0400'
---

Bike parties are a volunteer run, informal bike ride with lights, music, and fun. In short, Burning Man on a bicycle. Watch some of the videos below to see what they're like in other cities with hundreds (sometimes thousands) of cyclists attending.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_8AmPSPAaos?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/nha1YViNHas?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8-H0jnIg4pQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/6lYvxAroONc?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
