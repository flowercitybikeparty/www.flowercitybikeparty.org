---
layout: home
title:
date: '2018-05-15 22:31:30 -0400'
---
# Welcome!

Flower City Bike Party is a monthly bicycle ride around Rochester, NY. Our ride organizers are not talented web developers, so please forgive the basic look of our site. It's the best we could do with our monthly operating budget of $0 and limited free time. This website serves as a central location on the internet with information about our ride, how to join, and (in the future) when they will be.

All rides are posted to [Facebook](https://www.facebook.com/flowercitybikeparty/events/) and we do our best to post the ride route one week before the ride. Please read the information found in the links at the top of this page (mobile users should tap the chevron on the right).
