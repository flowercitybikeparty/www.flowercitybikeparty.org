---
layout: default
title: Disclaimer
permalink: /disclaimer/
date: '2018-05-15 21:34:40 -0400'
---
# Disclaimer
## Short version
Your ride leaders are not lawyers, nor can we afford group insurance. You ride at your own risk and assume all liability.

## Long version
Flower City Bike Party organizes bicycle rides on public roads shared with cars. Studies have repeatedly proven that motor vehicle traffic is the #1 cause of death amongst cyclists, even when cyclists are following all applicable road laws. Several hours of planning goes into every ride with multiple ride leaders and volunteers making sure no one is dropped, minor mechanical issues are fixed (when time and resources allow for it), the roads we take are relatively safe for bicycle traffic, and that intersections are corked when necessary for your safety. Even still, accidents can happen. Pay attention, wear a brain bucket, and don't sue your friendly ride leaders. Read up on [how we ride]({% link 3_rules.md %}).
