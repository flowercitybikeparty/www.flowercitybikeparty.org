---
layout: default
title: How We Ride
permalink: /rules/
date: '2018-05-15 21:54:56 -0400'
---
# How We Ride
## Dos
- Stay to the right-most lane when safe to do so, unless preparing for a left turn.
- Ride straight and predictably.
- Bring lights! It's a bike party. Make your bike look like it. [It's also a violation of NYS traffic law to ride a bike at night without lights](http://www.safeny.ny.gov/bike-vt.htm#sec1236) and there's a small number of RPD officers who will throw the book at you if they feel like it, in addition to it being recklessly unsafe.
- [Use your hand signals.](https://bikeleague.org/content/signaling)
- Stop at red lights and crosswalks.
- Pay attention to your ride leaders and follow their instructions.
- Wear a brain bucket. While not required, it's generally a good idea.

## Don'ts
- Ride against traffic.
- Leave any rider behind. If someone breaks down, please notify a ride leader or volunteer immediately.
- Antagonize or confront other drivers, even if they're being aggressive or reckless. Remember, you're not by yourself. Let ride organizers and volunteers deal with any confrontations.
- Pass the ride leader. They're tasked with determining when it is safe to cross intersections, controlling the speed of the ride, and corking intersections so everyone can make it through before traffic lights change.
- Be a jerk. This is a volunteer-run ride because we love Rochester and we love biking. We don't love dealing with jerks.
